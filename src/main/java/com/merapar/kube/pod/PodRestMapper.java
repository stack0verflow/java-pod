package com.merapar.kube.pod;

import com.merapar.kube.pod.model.PodRest;
import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.Pod;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class PodRestMapper {

    PodRest mapFromPod(Pod pod) {
        return new PodRest(
                pod.getMetadata().getName(),
                pod.getMetadata().getNamespace(),
                pod.getSpec().getContainers().stream().map(Container::getName).collect(Collectors.joining(", ")),
                pod.getSpec().getContainers().stream().map(Container::getImage).collect(Collectors.joining(", "))
        );
    }
}
