package com.merapar.kube.pod;

import com.merapar.kube.pod.model.PodRest;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("pod")
public class PodController {

    private final PodScheduleService podScheduleService;
    private final PodListService podListService;

    public PodController(PodScheduleService podScheduleService, PodListService podListService) {
        this.podScheduleService = podScheduleService;
        this.podListService = podListService;
    }

    @PostMapping
    public void schedule(@RequestBody @Valid PodRest configuration) {
        podScheduleService.schedulePod(configuration);
    }

    @GetMapping
    public List<PodRest> getPods() {
        return podListService.getPods();
    }
}
