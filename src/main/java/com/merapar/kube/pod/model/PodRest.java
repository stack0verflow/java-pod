package com.merapar.kube.pod.model;

import com.fasterxml.jackson.annotation.JsonCreator;

import javax.validation.constraints.NotEmpty;

public class PodRest {

    @NotEmpty
    private final String podName;
    @NotEmpty
    private final String namespace;
    @NotEmpty
    private final String containerName;
    @NotEmpty
    private final String image;

    @JsonCreator
    public PodRest(String podName, String namespace, String containerName, String image) {
        this.podName = podName;
        this.namespace = namespace;
        this.containerName = containerName;
        this.image = image;
    }

    public String getPodName() {
        return podName;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getContainerName() {
        return containerName;
    }

    public String getImage() {
        return image;
    }
}
