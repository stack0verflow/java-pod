package com.merapar.kube.pod;

import com.merapar.kube.pod.model.PodRest;
import io.fabric8.kubernetes.api.model.ContainerPortBuilder;
import io.fabric8.kubernetes.api.model.PodBuilder;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.springframework.stereotype.Service;

import javax.validation.Valid;

@Service
public class PodScheduleService {

    private final KubernetesClient client;

    public PodScheduleService(KubernetesClient client) {
        this.client = client;
    }

    void schedulePod(@Valid PodRest configuration) {
        client.pods().create(
                new PodBuilder()
                        .withNewMetadata()
                            .withName(configuration.getPodName())
                            .withNamespace(configuration.getNamespace())
                            .endMetadata()
                        .withNewSpec()
                            .addNewContainer()
                                .withName(configuration.getContainerName())
                                .withImage(configuration.getImage())
                                .withPorts(new ContainerPortBuilder().withContainerPort(8080).build())
                                .endContainer()
                            .endSpec()
                        .build());

    }
}
