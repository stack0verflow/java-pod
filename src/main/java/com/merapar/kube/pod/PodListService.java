package com.merapar.kube.pod;

import com.merapar.kube.pod.model.PodRest;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PodListService {

    private final KubernetesClient client;
    private final PodRestMapper podRestMapper;

    public PodListService(KubernetesClient client, PodRestMapper podRestMapper) {
        this.client = client;
        this.podRestMapper = podRestMapper;
    }

    List<PodRest> getPods() {
        return client.pods().list().getItems()
                .stream()
                .map(podRestMapper::mapFromPod)
                .collect(Collectors.toList());
    }
}
