FROM openjdk:11-jre-slim
EXPOSE 8080
ADD ./build/libs/kube-*.jar app.jar
ENTRYPOINT java -jar /app.jar
