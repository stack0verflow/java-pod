# Documentation
## Info
This repo consists of a simple project used for scheduling kubernetes pods

## Prerequisites 

- kubectl
- minikube

## How to run using kubectl

Run from project root:

>>>
kubectl run javak8s --image=szymonokroj/java-k8s --port=8080

kubectl apply -f admin-cluster-role-binding.yaml

kubectl expose deployment/javak8s --type="NodePort" --port 8080

export NODE_PORT=$(kubectl get services/javak8s -o go-template='{{(index .spec.ports 0).nodePort}}')

curl $(minikube ip):$NODE_PORT/pod
>>>

The last command should print your pod details:
>[{"podName":"javak8s-7bb74b8897-svbjq","namespace":"default","containerName":"javak8s","image":"szymonokroj/java-k8s"}]

## How to use
There are 2 available endpoints:

List all pods:
> GET /pod

Schedule new pod:
> POST /pod

Both endpoints use the same DTO, example:
```json
{
	"podName": "java-pod-from-inside",
	"namespace": "default",
	"containerName": "java-container",
	"image": "szymonokroj/gohello"
}
```